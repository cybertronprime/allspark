<?php
declare(strict_types=1);

namespace allspark\JWT;

class JWT
{
    use JWTvalidation;

    const ERROR_KEY_EMPTY        = 10;
    const ERROR_KEY_INVALID      = 12;
    const ERROR_ALGO_UNSUPPORTED = 20;
    const ERROR_ALGO_MISSING     = 22;
    const ERROR_INVALID_MAXAGE   = 30;
    const ERROR_INVALID_LEEWAY   = 32;
    const ERROR_JSON_FAILED      = 40;
    const ERROR_TOKEN_INVALID    = 50;
    const ERROR_TOKEN_EXPIRED    = 52;
    const ERROR_TOKEN_NOT_NOW    = 54;
    const ERROR_SIGNATURE_FAILED = 60;
    const ERROR_KID_UNKNOWN      = 70;

    protected $algos = [
        'HS256' => 'sha256',
        'HS384' => 'sha384',
        'HS512' => 'sha512',
        'RS256' => \OPENSSL_ALGO_SHA256,
        'RS384' => \OPENSSL_ALGO_SHA384,
        'RS512' => \OPENSSL_ALGO_SHA512,
    ];

    protected $key;
    protected $keys = [];
    protected $timestamp = null;
    protected $algo = 'HS512';
    protected $maxAge = 3600;
    protected $leeway = 0;
    protected $passphrase;

    public function __construct
    (
        $key,
        string $algo = 'HS512',
        int $maxAge = 3600,
        int $leeway = 0,
        string $pass = null
    )
    {
        $this->validateConfig($key, $algo, $maxAge, $leeway);

        if (\is_array($key))
        {
            $this->registerKeys($key);
            $key = \reset($key);
        }

        $this->key        = $key;
        $this->algo       = $algo;
        $this->maxAge     = $maxAge;
        $this->leeway     = $leeway;
        $this->passphrase = $pass;
    }

    public function registerKeys(array $keys): self
    {
        $this->keys = \array_merge($this->keys, $keys);
        return $this;
    }

    public function encode(array $payload, array $header = []): string
    {
        $header = ['typ' => 'JWT', 'alg' => $this->algo] + $header;

        $this->validateKid($header);

        if (!isset($payload['iat']) && !isset($payload['exp']))
        {
            $payload['exp'] = ($this->timestamp ?: \time()) + $this->maxAge;
        }

        $header    = $this->urlSafeEncode($header);
        $payload   = $this->urlSafeEncode($payload);
        $signature = $this->urlSafeEncode($this->sign($header . '.' . $payload));

        return $header . '.' . $payload . '.' . $signature;
    }

    public function decode(string $token): array
    {
        if (\substr_count($token, '.') < 2)
            throw new JWTException('Invalid token: Incomplete segments', static::ERROR_TOKEN_INVALID);

        $token = \explode('.', $token, 3);
        $this->validateHeader((array) $this->urlSafeDecode($token[0]));

        if (!$this->verify($token[0] . '.' . $token[1], $token[2]))
            throw new JWTException('Invalid token: Signature failed', static::ERROR_SIGNATURE_FAILED);

        $payload = (array) $this->urlSafeDecode($token[1]);
        $this->validateTimestamps($payload);
        return $payload;
    }

    public function setTestTimestamp(int $timestamp = null): self
    {
        $this->timestamp = $timestamp;
        return $this;
    }


    protected function sign(string $input): string
    {
        if (\substr($this->algo, 0, 2) === 'HS')
            return \hash_hmac($this->algos[$this->algo], $input, $this->key, true);

        $this->validateKey();

        \openssl_sign($input, $signature, $this->key, $this->algos[$this->algo]);

        return $signature;
    }

    protected function verify(string $input, string $signature): bool
    {
        $algo = $this->algos[$this->algo];

        if (\substr($this->algo, 0, 2) === 'HS')
            return \hash_equals($this->urlSafeEncode(\hash_hmac($algo, $input, $this->key, true)), $signature);

        $this->validateKey();
        $pubKey = \openssl_pkey_get_details($this->key)['key'];

        return \openssl_verify($input, $this->urlSafeDecode($signature, false), $pubKey, $algo) === 1;
    }

    protected function urlSafeEncode($data): string
    {
        if (\is_array($data))
        {
            $data = \json_encode($data, \JSON_UNESCAPED_SLASHES);
            $this->validateLastJson();
        }

        return \rtrim(\strtr(\base64_encode($data), '+/', '-_'), '=');
    }

    protected function urlSafeDecode($data, bool $asJson = true)
    {
        if (!$asJson)
            return \base64_decode(\strtr($data, '-_', '+/'));

        $data = \json_decode(\base64_decode(\strtr($data, '-_', '+/')));
        $this->validateLastJson();

        return $data;
    }
}

class JWTException extends \InvalidArgumentException
{

}

trait JWTvalidation
{
    protected function validateConfig($key, string $algo, int $maxAge, int $leeway)
    {
        if (empty($key))
            throw new JWTException('Signing key cannot be empty', static::ERROR_KEY_EMPTY);

        if (!isset($this->algos[$algo]))
            throw new JWTException('Unsupported algo ' . $algo, static::ERROR_ALGO_UNSUPPORTED);

        if ($maxAge < 1)
            throw new JWTException('Invalid maxAge: Should be greater than 0', static::ERROR_INVALID_MAXAGE);

        if ($leeway < 0 || $leeway > 120)
            throw new JWTException('Invalid leeway: Should be between 0-120', static::ERROR_INVALID_LEEWAY);
    }


    protected function validateHeader(array $header)
    {
        if (empty($header['alg']))
            throw new JWTException('Invalid token: Missing header algo', static::ERROR_ALGO_MISSING);

        if (empty($this->algos[$header['alg']]))
            throw new JWTException('Invalid token: Unsupported header algo', static::ERROR_ALGO_UNSUPPORTED);

        $this->validateKid($header);
    }

    protected function validateKid(array $header)
    {
        if (!isset($header['kid']))
            return;

        if (empty($this->keys[$header['kid']]))
            throw new JWTException('Invalid token: Unknown key ID', static::ERROR_KID_UNKNOWN);

        $this->key = $this->keys[$header['kid']];
    }


    protected function validateTimestamps(array $payload)
    {
        $timestamp = $this->timestamp ?: \time();
        $checks    = [
            ['exp', $this->leeway /*          */ , static::ERROR_TOKEN_EXPIRED, 'Expired'],
            ['iat', $this->maxAge - $this->leeway, static::ERROR_TOKEN_EXPIRED, 'Expired'],
            ['nbf', $this->maxAge - $this->leeway, static::ERROR_TOKEN_NOT_NOW, 'Not now'],
        ];

        foreach ($checks as list($key, $offset, $code, $error))
            if (isset($payload[$key]))
            {
                $offset += $payload[$key];
                $fail    = $key === 'nbf' ? $timestamp <= $offset : $timestamp >= $offset;

                if ($fail)
                    throw new JWTException('Invalid token: ' . $error, $code);
            }
    }


    protected function validateKey()
    {
        if (\is_string($key = $this->key))
        {
            if (\substr($key, 0, 7) !== 'file://')
                $key = 'file://' . $key;
            $this->key = \openssl_get_privatekey($key, $this->passphrase ?: '');
        }

        if (!\is_resource($this->key))
            throw new JWTException('Invalid key: Should be resource of private key', static::ERROR_KEY_INVALID);
    }


    protected function validateLastJson()
    {
        if (\JSON_ERROR_NONE === \json_last_error())
            return;
        throw new JWTException('JSON failed: ' . \json_last_error_msg(), static::ERROR_JSON_FAILED);
    }
}
