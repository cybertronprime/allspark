#!/bin/bash
source /etc/allspark/functions.sh
if [ -z $DOMAIN ]; then require DOMAIN string "Veuillez indiquer votre nom de domaine :"; source /root/.allspark; fi
if [ -z $MODULE_API_OPTIMUS ]; then require MODULE_API_OPTIMUS yesno "Voulez-vous installer le client OPTIMUS ?"; source /root/.allspark; fi
if [ -z $OPTIMUS_API_MARIADB_USER ]; then require OPTIMUS_API_MARIADB_USER string "Veuillez renseigner le nom de l'utilisateur MARIADB pour l'api OPTIMUS :"; source /root/.allspark; fi
if [ -z $OPTIMUS_API_MARIADB_PASSWORD ] || [ $OPTIMUS_API_MARIADB_PASSWORD = "auto" ]; then require OPTIMUS_API_MARIADB_PASSWORD password "Veuillez renseigner le mot de passe de l'utilisateur MARIADB pour l'api OPTIMUS :"; source /root/.allspark; fi
if [ -z $OPTIMUS_ADMIN_USER ]; then require OPTIMUS_ADMIN_USER string "Veuillez renseigner le nom de l'administrateur OPTIMUS :"; source /root/.allspark; fi
if [ -z $OPTIMUS_ADMIN_PASSWORD ] || [ $OPTIMUS_ADMIN_PASSWORD = "auto" ]; then require OPTIMUS_ADMIN_PASSWORD password "Veuillez renseigner le mot de passe de l'administrateur OPTIMUS :"; source /root/.allspark; fi
if [ -z $OPTIMUS_CREATE_FIRST_USER ]; then require OPTIMUS_CREATE_FIRST_USER yesno "Voulez-vous créer un premier utilisateur OPTIMUS ?"; source /root/.allspark; fi
if [[ $OPTIMUS_CREATE_FIRST_USER =~ ^[YyOo]$ ]] && [ -z $OPTIMUS_FIRST_USER ]; then require OPTIMUS_FIRST_USER string "Veuillez renseigner le nom du premier utilisateur OPTIMUS :"; source /root/.allspark; fi
if [[ $OPTIMUS_CREATE_FIRST_USER =~ ^[YyOo]$ ]] && { [ -z $OPTIMUS_FIRST_PASSWORD ] || [ $OPTIMUS_FIRST_PASSWORD = "auto" ]; }; then require OPTIMUS_FIRST_PASSWORD password "Veuillez renseigner le mot de passe de connexion du premier utilisateur OPTIMUS :"; source /root/.allspark; fi
if [ -z $AES_KEY ] || [ $AES_KEY = "auto" ]; then require AES_KEY aeskey "Veuillez renseigner une clé de chiffrement AES de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi
if [ -z $API_SHA_KEY ] || [ $API_SHA_KEY = "auto" ]; then require API_SHA_KEY aeskey "Veuillez renseigner une clé de chiffrement SHA de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi

source /root/.allspark

if [ $MODULE_API_OPTIMUS = "Y" ]
then

  echo
  echo_green "==== INSTALLATION DE L'API OPTIMUS ===="

  echo_magenta "Installation de libreoffice (pour conversions de fichiers doc, odt, xls, pdf) ..."
  verbose apt-get -qq install libreoffice

  echo_magenta "Installation de pdftk (manipulation de documents pdf) ..."
  verbose apt-get -qq install pdftk
  

  echo_magenta "Creation de l'utilisateur MARIADB"
  verbose mariadb -u root -e "GRANT SELECT ON users.users TO '$OPTIMUS_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$OPTIMUS_API_MARIADB_PASSWORD';"
  verbose mariadb -u root -e "FLUSH PRIVILEGES;"
  
  echo_magenta "Creation de l'utilisateur 'admin' OPTIMUS"
  verbose mariadb -u root -e "INSERT IGNORE INTO users.users VALUES (1, 1, 1, '$OPTIMUS_ADMIN_USER', AES_ENCRYPT('$OPTIMUS_ADMIN_PASSWORD','$AES_KEY'));"
  
  echo_magenta "installation du module api optimus"
  verbose rm -f -R /srv/api/api_optimus
  verbose mkdir /srv/api/api_optimus
  sudo git clone --quiet https://git.cybertron.fr/optimus/api /srv/api/api_optimus/.
  sed -i "s/OPTIMUS_API_MARIADB_USER/$OPTIMUS_API_MARIADB_USER/g" /srv/api/api_optimus/config.php
  sed -i "s/OPTIMUS_API_MARIADB_PASSWORD/$OPTIMUS_API_MARIADB_PASSWORD/g" /srv/api/api_optimus/config.php
  chown -R www-data:www-data /srv/api/api_optimus
  
  echo_magenta "Création d'une tâche automatique qui s'exécute chaque minute"
  cp /etc/allspark/optimus/optimus-api.timer /etc/systemd/system/optimus-api.timer
  cp /etc/allspark/optimus/optimus-api.service /etc/systemd/system/optimus-api.service
  systemctl enable optimus-api.timer
  systemctl start optimus-api.timer
  systemctl daemon-reload

  if [ $OPTIMUS_CREATE_FIRST_USER = "Y" ]
  then
    echo_magenta "Creation du premier utilisateur OPTIMUS"
    
    verbose mariadb -u root -e "INSERT IGNORE INTO users.users VALUES (2 , 1, 0, '$OPTIMUS_FIRST_USER@$DOMAIN', AES_ENCRYPT('$OPTIMUS_FIRST_PASSWORD','$AES_KEY'));"
    verbose mariadb -u root -e "FLUSH PRIVILEGES;"
    
    echo_magenta "Création de la boite mail '$OPTIMUS_FIRST_USER@$DOMAIN'"
    verbose mariadb -u root -e "INSERT IGNORE INTO mailserver.mailboxes VALUES (2, '$OPTIMUS_FIRST_USER@$DOMAIN', 0, 1, null, null, null, null, null);"
    verbose mkdir "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN"
    verbose mkdir "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/==DOSSIERS=="
    verbose touch "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/subscriptions"
    chmod +770 "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN"
    chmod +770 "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/==DOSSIERS=="
    chmod +770 "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/subscriptions"
    chown mailboxes:mailboxes "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN"
    chown mailboxes:mailboxes "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/==DOSSIERS=="
    chown mailboxes:mailboxes "/srv/mailboxes/$OPTIMUS_FIRST_USER@$DOMAIN/subscriptions"

    echo_magenta "Création du dossier /srv/files/$OPTIMUS_FIRST_USER@$DOMAIN et ses sous-dossiers"
    verbose mkdir "/srv/files/$OPTIMUS_FIRST_USER@$DOMAIN"
    verbose chown www-data:www-data "/srv/files/$OPTIMUS_FIRST_USER@$DOMAIN"
	
	echo_magenta "Creation de la première structure d'exercice OPTIMUS"
	verbose mariadb -u root -e "INSERT IGNORE INTO users.users VALUES (3, 1, 0, 'structure@$DOMAIN', AES_ENCRYPT('$OPTIMUS_FIRST_PASSWORD','$AES_KEY'));"
	verbose mariadb -u root -e "INSERT IGNORE INTO cloud.groupmembers VALUES (1, 3, 2);"
	verbose mariadb -u root -e "FLUSH PRIVILEGES;"
	echo_magenta "Création du dossier /srv/files/structure@$DOMAIN et ses sous-dossiers"
    verbose mkdir "/srv/files/structure@$DOMAIN"
	verbose mkdir "/srv/files/structure@$DOMAIN/==MODELES FACTURES=="
	verbose cp "/srv/api/api_optimus/templates/Facture.doc" "/srv/files/structure@$DOMAIN/==MODELES FACTURES==/Facture.doc" 
	verbose cp "/srv/api/api_optimus/templates/Facture.odt" "/srv/files/structure@$DOMAIN/==MODELES FACTURES==/Facture.odt" 
	verbose cp "/srv/api/api_optimus/templates/Facture.pdf" "/srv/files/structure@$DOMAIN/==MODELES FACTURES==/Facture.pdf" 
    chown -R www-data:www-data "/srv/files/structure@$DOMAIN"

    echo_magenta "Installation de la base de données '$OPTIMUS_FIRST_USER@$DOMAIN'"
    verbose mariadb -u root -e "CREATE DATABASE IF NOT EXISTS \`$OPTIMUS_FIRST_USER@$DOMAIN\` CHARACTER SET utf8 COLLATE utf8_general_ci;"
    verbose mariadb -u root -e "GRANT ALL PRIVILEGES ON \`$OPTIMUS_FIRST_USER@$DOMAIN\`.* TO '$OPTIMUS_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$OPTIMUS_API_MARIADB_PASSWORD';"
    verbose mariadb -u root -e "USE \`$OPTIMUS_FIRST_USER@$DOMAIN\`;"
    for file in /srv/api/api_optimus/sql/user/*.sql
    do
      file="${file:30:-4}"
      echo_magenta "--> $file.sql exécuté"
      mariadb -f "$OPTIMUS_FIRST_USER@$DOMAIN" < /srv/api/api_optimus/sql/user/$file.sql
    done

    echo_magenta "Configuration de la structure d'exercice '$DOMAIN'"
    verbose mariadb -u root -e "CREATE DATABASE IF NOT EXISTS \`structure@$DOMAIN\` CHARACTER SET utf8 COLLATE utf8_general_ci;"
    verbose mariadb -u root -e "GRANT ALL PRIVILEGES ON \`structure@$DOMAIN\`.* TO '$OPTIMUS_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$OPTIMUS_API_MARIADB_PASSWORD';"
    verbose mariadb -u root -e "USE \`structure@$DOMAIN\`;"
    for file in /srv/api/api_optimus/sql/structure/*.sql
    do
      file="${file:35:-4}"
      echo_magenta "--> $file.sql exécuté"
      mariadb -f "structure@$DOMAIN" < /srv/api/api_optimus/sql/structure/$file.sql
    done

    verbose mariadb -u root -e "REPLACE INTO \`$OPTIMUS_FIRST_USER@$DOMAIN\`.structures VALUES ('1','$DOMAIN','structure@$DOMAIN','2020-11-01',NULL);"
    verbose mariadb -u root -e "REPLACE INTO \`structure@$DOMAIN\`.members VALUES ('1','$DOMAIN','$OPTIMUS_FIRST_USER@$DOMAIN');"
    verbose mariadb -u root -e "FLUSH PRIVILEGES;"
  fi

fi
