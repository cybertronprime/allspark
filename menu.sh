#!/bin/bash
source /etc/allspark/functions.sh
source /root/.allspark

DOMAIN_TO_DNS=$( getent hosts $DOMAIN | awk '{ print $1 }' )
PUBLIC_IP=$( wget -qO- ipinfo.io/ip )

while : ; do

clear

tput cup 2 	3; echo -ne  "\033[46;30m          ALLSPARK INSTALLER          \e[0m"
tput cup 3 	3; echo -ne  "\033[46;30m                 V1.63                \e[0m"

tput cup 5  3; if [ -n "$LAST_UPGRADE" ]; then echo_green "a. Mettre à jour le système (LASTUPGRADE : $LAST_UPGRADE)"; else echo_red "a. Mettre à jour le système"; fi
tput cup 6  3; if [ -n "$PART_TO_ENCRYPT" ] && lsblk -o NAME -n /dev/$PART_TO_ENCRYPT 2>/dev/null | grep -q $PART_TO_ENCRYPT; then echo_green "b. Créer une partition $PART_TO_ENCRYPT indépendante"; else echo_red "b. Créer une partition $PART_TO_ENCRYPT indépendante"; fi
tput cup 7  3; if /sbin/blkid /dev/$PART_TO_ENCRYPT 2>/dev/null | grep -q 'crypto_LUKS'; then echo_green "c. Activer le chiffrement sur la partition $PART_TO_ENCRYPT"; else echo_red "c. Activer le chiffrement sur la partition $PART_TO_ENCRYPT"; fi
tput cup 8  3; if lsblk -o MOUNTPOINT -n /dev/mapper/crypt$PART_TO_ENCRYPT 2>/dev/null | grep -q '/srv'; then echo_green "d. Déchiffrer la partition $PART_TO_ENCRYPT et la monter sur /srv"; else echo_red "d. Déchiffrer la partition $PART_TO_ENCRYPT et la monter sur /srv"; fi
tput cup 9  3; if grep -q "Port 7822" /etc/ssh/sshd_config; then echo_green "e. Sécuriser le serveur"; else echo_red "e. Sécuriser le serveur"; fi
tput cup 10 3; if [ -d "/etc/apache2" ]; then echo_green "f. Installer le serveur web APACHE"; else echo_red "f. Installer le serveur web APACHE"; fi
tput cup 11 3; if [ -d "/srv/www" ]; then echo_green "g. Installer l'espace d'hébergement www"; else echo_red "g. Installer l'espace d'hébergement www"; fi
tput cup 12 3; if [ -d "/etc/php" ]; then echo_green "h. Installer PHP"; else echo_red "h. Installer PHP"; fi
tput cup 13 3; if [ -d "/etc/mysql" ]; then echo_green "i. Installer MARIADB"; else echo_red "i. Installer MARIADB"; fi
tput cup 14 3; if [ -d "/etc/dovecot" ]; then echo_green "j. Installer le serveur mail"; else echo_red "j. Installer le serveur mail"; fi
tput cup 15 3; if [ -d "/srv/webmail" ]; then echo_green "k. Installer le webmail ROUNDCUBE"; else echo_red "k. Installer le webmail ROUNDCUBE"; fi
tput cup 16 3; if [ -d "/srv/cloud" ]; then echo_green "l. Installer le serveur cloud SABREDAV (WEBDAV)"; else echo_red "l. Installer le serveur cloud SABREDAV (WEBDAV)"; fi
tput cup 17 3; if [ -d "/srv/api/api_allspark" ]; then echo_green "m. Installer l'api ALLSPARK"; else echo_red "m. Installer l'api ALLSPARK"; fi
tput cup 18 3; if [ -d "/srv/shared" ]; then echo_green "n. Installer l'espace de partage de fichiers"; else echo_red "n. Installer l'espace de partage de fichiers"; fi
tput cup 19 3; if [ -d "/srv/api/api_optimus" ]; then echo_green "o. Installer l'api OPTIMUS"; else echo_red "o. Installer l'api OPTIMUS"; fi
tput cup 20 3; if [[ $DOMAIN_TO_DNS == $PUBLIC_IP ]]; then echo_green "p. Configuration de la zone DNS"; else echo_red "p. Configuration de la zone DNS"; fi
tput cup 21 3; if [ -f "/etc/letsencrypt/live/$DOMAIN/privkey.pem" ]; then echo_green "q. Installer les certificats SSL"; else echo_red "q. Installer les certificats SSL"; fi

tput cup 23 3; if [ -f "/srv/allspark-backup.sh" ]; then echo_green "r. Installer les scripts de sauvegarde"; else echo_red "r. Installer les scripts de sauvegarde"; fi

tput cup 25 3; echo_green "s. Sauvegarder la configuration et les clés de chiffrement"

tput cup 27 3; echo_green "t. Editer la configuration"
tput cup 28 3; echo_green "u. Mettre à jour AllSpark Installer"
tput cup 29 3; echo_green "v. Redémarrer le serveur"
tput cup 30 3; echo_green "w. Afficher le QR CODE 2FA"
tput cup 31 3; echo_green "x. Quitter"

tput cup 33 3; echo_green "y. INSTALLATION GUIDEE"
tput cup 34 3; echo_green "z. INSTALLATION AUTOMATISEE"

tput cup 36 3; echo -ne "\033[46;30m Select Option : \e[0m"; tput cup 25 21

tput cup 40 3; echo_magenta "Il est rappelé que les scripts d'installation ALLSPARK ainsi que le logiciel OPTIMUS sont des logiciels libres."
tput cup 41 3; echo_magenta "Le texte complet de la licence GNU AGPL V3 est fourni dans le fichier LICENSE ou consultable en tapant [ESPACE]."
tput cup 42 3; echo_magenta "Cela signifie que vous les utilisez sous votre seule et unique responsabilité."
tput cup 43 3; echo_magenta "Personne ne peut être tenu pour responsable d'un quelconque dommage, notamment lié à une perte de vos données"

read -n 1 y

case "$y" in

  a)
		tput reset
		clear
    source /etc/allspark/upgrade/install.sh
    source /root/.allspark
    read -p "Appuyez sur [ENTREE] pour continuer..."
		;;

  b)
		tput reset
		clear
		source /etc/allspark/diskpart/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
		;;

  c)
  	tput reset
  	clear
  	source /etc/allspark/crypt/install.sh
  	read -p "Appuyez sur [ENTREE] pour continuer..."
  	;;

  d)
    tput reset
    clear
    source /etc/allspark/decrypt/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  e)
    tput reset
    clear
    source /etc/allspark/secure/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  f)
    tput reset
    clear
    source /etc/allspark/apache/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  g)
    tput reset
    clear
    source /etc/allspark/www/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  h)
    tput reset
    clear
    source /etc/allspark/php/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  i)
    tput reset
    clear
    source /etc/allspark/mariadb/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  j)
    tput reset
    clear
    source /etc/allspark/mailserver/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  k)
    tput reset
    clear
    source /etc/allspark/webmail/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  l)
    tput reset
    clear
    source /etc/allspark/cloud/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  m)
    tput reset
    clear
    source /etc/allspark/api/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  n)
    tput reset
    clear
    source /etc/allspark/shared/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  o)
    tput reset
    clear
    source /etc/allspark/optimus/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  p)
    tput reset
    clear
    source /etc/allspark/zonedns/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  q)
    tput reset
    clear
    source /etc/allspark/letsencrypt/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  r)
    tput reset
    clear
    source /etc/allspark/backup/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  s)
    tput reset
    clear
    source /etc/allspark/saveconfig/install.sh
    ;;

  t)
    tput reset
    clear
    nano /root/.allspark
    source /etc/allspark/menu.sh
    ;;

  u)
		tput reset
		clear
    source /etc/allspark/update/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
		;;

  v)
    tput reset
    reboot
    exit 1
    ;;

  w)
    tput reset
    qrencode -t ansi "otpauth://totp/debian@$DOMAIN?secret=${SECURE_GOOGLEAUTH_KEY}&issuer=ALLSPARK"
    echo
    echo_magenta "Ce code doit être scanné et conservé sur votre smartphone à l'aide d'une application comme GOOGLE Authenticator, 2FAS ou Authy (gratuit)"
    echo_magenta "Ces applications permettent de générer un mot de passe qui change toutes les 30 secondes et qui vous sera demandé pour vous authentifier sur le serveur"
    echo
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  x)
    tput reset
    clear
    exit 1
    ;;

  y)
    tput reset
    clear
    source /etc/allspark/upgrade/install.sh
  	source /etc/allspark/diskpart/install.sh
    source /etc/allspark/crypt/install.sh
    #source /etc/allspark/decrypt/install.sh
    source /etc/allspark/secure/install.sh
    source /etc/allspark/apache/install.sh
    source /etc/allspark/www/install.sh
    source /etc/allspark/php/install.sh
    source /etc/allspark/mariadb/install.sh
    source /etc/allspark/mailserver/install.sh
    source /etc/allspark/webmail/install.sh
    source /etc/allspark/cloud/install.sh
    source /etc/allspark/api/install.sh
    source /etc/allspark/shared/install.sh
    source /etc/allspark/optimus/install.sh
    source /etc/allspark/backup/install.sh
    qrencode -t ansi "otpauth://totp/debian@$DOMAIN.fr?secret=${SECURE_GOOGLEAUTH_KEY}&issuer=ALLSPARK"
    read -p "Appuyez sur [ENTREE] après avoir enregistré votre code ..."
    clear
    source /etc/allspark/zonedns/install.sh
    read -p "Appuyez sur [ENTREE] après avoir modifié votre enregistrement DNS, configuré le reverse DNS, puis ouvert les ports requis..."
    clear
    source /etc/allspark/letsencrypt/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer..."
    ;;

  z)
  	tput reset
  	clear
    if [ -z $DOMAIN ]; then require DOMAIN string "Veuillez renseigner votre nom de domaine :"; fi
    if [ -z $OPTIMUS_FIRST_USER ]; then require OPTIMUS_FIRST_USER string "Merci d'indiquer le nom du premier utilisateur OPTIMUS à créer. Exemple 'alice.dupont' pour créer 'alice.dupont@$DOMAIN :"; source /root/.allspark; fi
    if [ -z $OPTIMUS_FIRST_PASSWORD ]; then require OPTIMUS_FIRST_PASSWORD password "Merci d'indiquer le mot de passe de l'utilisateur OPTIMUS '$OPTIMUS_FIRST_USER'. Au moins 9 caractères, 2 chiffres, 1 majuscule et 1 caractère spécial :"; fi
    update_conf VERBOSE 2
    update_conf AES_KEY auto
    update_conf WEBMAIL_DES_KEY auto
    update_conf API_SHA_KEY auto
    update_conf UUID auto
    update_conf MODULE_APACHE "Y"
    update_conf MODULE_API "Y"
    update_conf MODULE_BACKUP "N"
    update_conf MODULE_CLOUD "Y"
    update_conf MODULE_SHARED "Y"
    update_conf MODULE_CRYPT "Y"
    update_conf MODULE_DECRYPT "Y"
    update_conf MODULE_DISKPART "Y"
    update_conf MODULE_LETSENCRYPT "Y"
    update_conf MODULE_MAILSERVER "Y"
    update_conf MODULE_MARIADB "Y"
    update_conf MODULE_MARIADB_REMOTE_ACCESS "Y"
    update_conf MODULE_API_OPTIMUS "Y"
    update_conf MODULE_PHP "Y"
    update_conf MODULE_SECURE_UPDATE "Y"
    update_conf MODULE_SECURE_ENABLEFW "Y"
    update_conf MODULE_SECURE_FAIL2BAN "Y"
    update_conf MODULE_SECURE_CHANGEROOTPASS "Y"
    update_conf MODULE_SECURE_CHANGEDEBIANPASS "Y"
    update_conf MODULE_SECURE_SSH_REPLACEDEFAULTPORT "Y"
    update_conf MODULE_SECURE_SSH_PORTKNOCKING "N"
    update_conf MODULE_SECURE_SSH_PORTKNOCKING_SEQUENCE "1083,1080,1082,1075"
    update_conf MODULE_SECURE_SSH_DISABLEROOTACCESS "Y"
    update_conf MODULE_SECURE_SSH_2FA "Y"
    update_conf MODULE_SHARED "Y"
    update_conf MODULE_UPGRADE "Y"
    update_conf MODULE_WEBMAIL "Y"
    update_conf MODULE_WWW "Y"
    update_conf AUTODECRYPT "Y"
    update_conf SECURE_ROOT_PASSWORD auto
    update_conf SECURE_DEBIAN_PASSWORD auto
    update_conf MARIADB_ADMIN_USER "mariadb"
    update_conf MARIADB_ADMIN_PASSWORD auto
    update_conf MARIADB_REMOTE_ROOT_PASSWORD auto
    update_conf ALLSPARK_API_MARIADB_USER "allspark-api"
    update_conf ALLSPARK_API_MARIADB_PASSWORD auto
    update_conf OPTIMUS_API_MARIADB_USER "optimus-api"
    update_conf OPTIMUS_API_MARIADB_PASSWORD auto
    update_conf OPTIMUS_ADMIN_USER "prime@$DOMAIN"
    update_conf OPTIMUS_ADMIN_PASSWORD auto
    update_conf OPTIMUS_CREATE_FIRST_USER "Y"
    update_conf CLOUD_MARIADB_USER cloud
    update_conf CLOUD_MARIADB_PASSWORD auto
    update_conf MAILSERVER_MARIADB_USER mailboxes
    update_conf MAILSERVER_MARIADB_PASSWORD auto
    update_conf MAILSERVER_POSTMASTER_USER "prime@$DOMAIN"
    update_conf MAILSERVER_POSTMASTER_PASSWORD auto
    source /root/.allspark
    source /etc/allspark/upgrade/install.sh
  	source /etc/allspark/diskpart/install.sh
    source /etc/allspark/crypt/install.sh
    #source /etc/allspark/decrypt/install.sh
    source /etc/allspark/secure/install.sh
    source /etc/allspark/apache/install.sh
    source /etc/allspark/www/install.sh
    source /etc/allspark/php/install.sh
    source /etc/allspark/mariadb/install.sh
    source /etc/allspark/mailserver/install.sh
    source /etc/allspark/webmail/install.sh
    source /etc/allspark/cloud/install.sh
    source /etc/allspark/api/install.sh
    source /etc/allspark/shared/install.sh
    source /etc/allspark/optimus/install.sh
    source /etc/allspark/backup/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer ..."
    clear
    qrencode -t ansi "otpauth://totp/debian@$DOMAIN?secret=${SECURE_GOOGLEAUTH_KEY}&issuer=ALLSPARK"
    echo
    echo_magenta "Ce code doit être scanné et conservé sur votre smartphone à l'aide d'une application comme GOOGLE Authenticator, 2FAS ou Authy (gratuit)"
    echo_magenta "Ces applications permettent de générer un mot de passe qui change toutes les 30 secondes et qui vous sera demandé pour vous authentifier sur le serveur"
    echo
    read -p "Appuyez sur [ENTREE] pour continuer..."
    clear
    source /etc/allspark/zonedns/install.sh
    read -p "Appuyez sur [ENTREE] après avoir modifié votre enregistrement DNS, configuré le reverse DNS, puis ouvert les ports requis..."
    clear
    source /etc/allspark/letsencrypt/install.sh
    read -p "Appuyez sur [ENTREE] pour continuer ..."
    clear
    echo
    echo_magenta "Il est rappelé que les scripts d'installation ALLSPARK ainsi que le logiciel OPTIMUS sont des logiciels libres."
    echo_magenta "Le texte complet de la licence GNU AGPL V3 est fourni dans le fichier LICENSE ou consultable sur https://git.cybertron.fr/optimus/allspark/-/raw/master/LICENSE"
    echo_magenta "Cela signifie que vous les utilisez sous votre seule et unique responsabilité."
    echo_magenta "Personne ne peut être tenu pour responsable d'un quelconque dommage, notamment lié à une perte de vos données"
    echo "APPUYER SUR [ENTREE] POUR CONTINUER"
    read -p ""
    clear
    source /etc/allspark/saveconfig/install.sh
    clear
    echo_magenta "Un redémarrage est nécessaire pour finaliser l'installation"
    echo_magenta "Avant toute utilisation vérifiez bien que vous parvenez toujours à accéder au serveur via SSH après le redémarrage"
    echo_magenta "Si vous avez choisi de renforcer la sécurité en modifiant le port SSH, rappelez vous que la connexion se fait désormais via le port 7822"
    echo_magenta "Si vous avez choisi de renforcer la sécurité en ajoutant une authentification 2FA, vérifiez bien que l'accès fonctionne"
    echo_magenta "Après le redémarrage du serveur, vérifiez que la partition chiffrée a bien été ouverte (option 'd' du menu en vert)"
    echo "APPUYER SUR [ENTREE] POUR CONTINUER"
	read -p ""
    echo
    echo_green "Si l'installation s'est bien déroulée, vous pourrez vous connecter à https://beta.optimus-avocats.fr avec les identifiants $DOMAIN / $OPTIMUS_FIRST_USER@$DOMAIN / $OPTIMUS_FIRST_PASSWORD"
    echo_green "ou avec les identifiants de l'administrateur $OPTIMUS_ADMIN_USER@$DOMAIN / $OPTIMUS_ADMIN_PASSWORD qui a pouvoir de créer d'autres utilisateurs"
    echo "APPUYER SUR [ENTREE] POUR REDEMARRER"
    read -p ""
    reboot
    ;;

  '')
    clear
    more /etc/allspark/LICENSE
    ;;
esac
done
